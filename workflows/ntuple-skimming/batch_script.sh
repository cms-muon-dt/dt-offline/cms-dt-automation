#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/c/cmsdtaut/

kinit cmsdtaut -k -t cmsdtaut.keytab

source /cvmfs/cms.cern.ch/cmsset_default.sh

cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Execute the actual processing here, this can be a CMSSW job or anything else (cmsRun is just an example)
# run_number:{d},fill:{d}

mkdir -p skimmed
cd skimmed
INFILES=`echo $INFILE | tr ',' ' '`
echo $INFILES
skimTree -c '(seg_wheel==2 && seg_sector==12) || (ph2Seg_wheel==2 && ph2Seg_sector==12)' "${INFILES}" ntuple_${JOBID}_skim.root

cd ..
ls -trla
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    mkdir -p $EOSDIR/skimmed/
    cp skimmed/ntuple_*.root $EOSDIR/skimmed/
    OFILE=`ls skimmed/ntuple_*.root`
    OFILE=$EOSDIR/skimmed/`basename $OFILE`
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
