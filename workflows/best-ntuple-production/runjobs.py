#!/usr/bin/env python3
import sys
from typing import Optional, Dict, List, Any, Union
from omsapi import OMSAPI, OMS_FILTER_OPERATORS
from ecalautoctrl import HTCHandler, dbs_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase


def process_by_best_run(cls):
    """
    This decorator adds the a :func:`groups` function that implements splitting by fill.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    :param fill_complete: if set to `True` wait for the fill to be complete and all the data to
    be available. Otherwise return all the runs currently available in each fill. The second
    mode can be (which is the default) is useful to process data per fill in a "growing" dataset
    fashion.
    """
    def groups(self) -> List[List[Dict]]:
        """
        Group runs by fill and select the best run for each fill.

        This function is added by :deco:`~ecalautoctrl.process_by_best_run`.
        """
        self.wdeps.update({self.task: 'new'})
        runs = self.rctrl.getRuns(status=self.wdeps)
        self.wdeps.update({self.task: 'reprocess'})
        runs.extend(self.rctrl.getRuns(status=self.wdeps))


        grps = []
        fills = {}
        for run in runs:
            q = self.rctrl.oms.query('runs')
            q.sort('run_number', asc=True)
            q.paginate(page=1, per_page=10000)
            q.filter('run_number', int(run['run_number']))
            q.attrs(['duration'])
            run['duration'] = q.data().json()['data'][-1]['attributes']["duration"]
            if run['fill'] in fills:
                fills[run['fill']].append(run)
            else:
                fills[run['fill']] = [run]

        self.log.info(f"Found {len(fills)} fills.")
        # remove fills for which previous step is not complete:
        # - check for fill that are ongoing
        # - check that the number of runs from getRuns equals the total number fo runs
        #   in a given fill.
        for fill, rr in fills.items():
            if self.rctrl.fillDumped(fill=fill) and (len(self.rctrl.getRunsInFill(fill=fill, task=self.task)) == len(rr)):
                self.log.info(f"Found {len(rr)} runs in fill {fill}.")
                best_run = [rr[0]]
                for run in rr:
                    if run['lumi'] > best_run[0]['lumi']:
                        best_run = [run]
                if best_run[0]['lumi']==0:
                    for run in rr:
                        if run['duration'] > best_run[0]['duration']:
                            best_run = [run]
                
                self.log.info(f"Best run for fill {fill} is {best_run[0]['run_number']}.")
                grps.append(best_run)
                self.log.info(grps)
        return grps

    setattr(cls, 'groups', groups)
    return cls

@dbs_data_source
@process_by_best_run
class HTCHandlerBestRunDBS(HTCHandler):
    """
    MISSING
    """

    def __init__(self,
                 task: str,
                 dsetname: Union[str, List[str]],
                 deps_tasks: Optional[List[str]] = None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.dsetname = dsetname

                
if __name__ == '__main__':

    handler = HTCHandlerBestRunDBS(task='best-ntuple-production',
                                   dsetname="/Express*/*-Express-*/FEVT")
    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerBestRunDBS)
