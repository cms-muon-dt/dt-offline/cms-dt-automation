#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    # t0lock0 = T0ProcDatasetLock(dataset='/Express*', stage='Express')

    handler = HTCHandlerByRunDBS(task='ntuple-production',
                                 dsetname="/Express*/*-Express-*/FEVT")    
    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
