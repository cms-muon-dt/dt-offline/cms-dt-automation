#!/usr/bin/env python3
import sys
import os
import runregistry
from typing import List, Dict
from ecalautoctrl import HTCHandlerByRunDBS, T0ProcDatasetLock
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase, LockBase

class RunRegistryLock(LockBase):
    def __init__(self, **kwargs):
        super().__init__()
        os.environ['SSO_CLIENT_ID'] = 'ecalgit-omsapi'
        os.environ['SSO_CLIENT_ID'] = 'KXbuy4vxiETBc5C7FwteQxAF3X1irilx'
    
    def lock(self, runs: List[Dict])->List[bool]:
        locks = len(runs)*[True]
        for i,run in enumerate(runs):
            rr_run_attributes = runregistry.get_run(run_number=run['run_numbers'])
            
        return locks
        

if __name__ == '__main__':
    # t0lock0 = T0ProcDatasetLock(dataset='/Express*', stage='Express')

    handler = HTCHandlerByRunDBS(task='rr-ntuple-production',
                                 dsetname="/Express*/*-Express-*/FEVT")    
    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRunDBS)
