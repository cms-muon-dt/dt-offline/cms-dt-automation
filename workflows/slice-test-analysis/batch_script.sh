#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/c/cmsdtaut/

kinit cmsdtaut -k -t cmsdtaut.keytab

source /cvmfs/cms.cern.ch/cmsset_default.sh

cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Execute the actual processing here, this can be a CMSSW job or anything else (cmsRun is just an example)
# TASK = run_number:{d},fill:{d}
# get run_number with `echo $TASK | grep -Po '\d+' | head -n1`

mkdir -p analysis
cd analysis

export RUN_NUMBER=`echo $TASK | grep -Po '\d+'| head -n1`

echo runSliceTestAnalysis $RUN_NUMBER $INFILE

runSliceTestAnalysis $RUN_NUMBER $INFILE
sh ../plotAndPublish.sh "run${RUN_NUMBER}"

ls -trla
cd ..

RETCMSSW=$?

RET=$(echo "$RETCMSSW" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
