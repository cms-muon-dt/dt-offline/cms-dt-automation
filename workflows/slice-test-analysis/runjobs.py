#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRun
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

if __name__ == '__main__':
    handler = HTCHandlerByRun(task='slice-test-analysis',
                              prev_input='ntuple-skimming')    
    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerByRun)
