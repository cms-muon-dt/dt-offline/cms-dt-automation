#!/usr/bin/env groovy
package dtctrl.vars // Change 'examplectrl' to something specific for eash subsystem, e.g. 'dtctrl'

class GlobalVars{
    static def setenv(script, jobname)
    {   
        // Edit this with your subsystem variables
        switch(jobname) {
            case ~/.*-prod$/:
                script.env.rctrl_project = "rctrl-prod"  
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-muon-dt/dt-offline/cms-dt-automation:prod" // Your unpacked docker image  
                script.env.image_setup = "source /home/cmsdtaut/setup.sh; module load lxbatch/tzero" // Your image setup script
                script.env.dbinstance = "dt_test_v1" // The InfluxDB where you write - usually different between prod and repro
                script.env.campaign = "express" // The campaign tag, can also be a list of campaigns with a space in between
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3tgrptpc5ibgufpwbtfwqnzksr" // The mattermost hook for notification - optional functionality
                script.env.logurl = "https://cms-dt-automation.web.cern.ch/logs" // The URL where the logs are published - defaults to ecallogs - optional functionality
                script.env.eospath = "/eos/cms/store/group/dpg_dt/comm_dt/automation" // Where you want to save stuff etc...
                script.env.eosplots = "/eos/project/c/cmsweb/www/MUON/dpgdt/sx5/Results/Automation" // Where you want to save plots etc... - optional functionality
                script.env.plotsurl = "https://dt-sx5.web.cern.ch/dt-sx5/Results/Automation/" // URL were the plots are plublished - optional functionality
                break
            case ~/.*-repro$/:
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-muon-dt/dt-offline/cms-dt-automation:dev"
                script.env.image_setup = "source /home/cmsdtaut/setup.sh"
                script.env.dbinstance = "dt_test_v1"
                script.env.campaign = "all"
                script.env.logurl = "https://cms-dt-automation.web.cern.ch/logs"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3tgrptpc5ibgufpwbtfwqnzksr"
                script.env.eospath = "/eos/cms/store/group/dpg_dt/comm_dt/automation"
                script.env.eosplots = "/eos/project/c/cmsweb/www/MUON/dpgdt/sx5/Results/Automation"
                script.env.plotsurl = "https://dt-sx5.web.cern.ch/dt-sx5/Results/Automation/"
                break
            case ~/.*-test$/:
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-muon-dt/dt-offline/cms-dt-automation:dev"
                script.env.image_setup = "source /home/cmsdtaut/setup.sh"
                script.env.dbinstance = "dt_test_v1"
                script.env.campaign = "campaign_tesi"
                script.env.logurl = "https://cms-dt-automation.web.cern.ch/logs"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3tgrptpc5ibgufpwbtfwqnzksr"
                script.env.eospath = "/eos/cms/store/group/dpg_dt/comm_dt/automation"
                script.env.eosplots = "/eos/project/c/cmsweb/www/MUON/dpgdt/sx5/Results/Automation"
                script.env.plotsurl = "https://dt-sx5.web.cern.ch/dt-sx5/Results/Automation/"
                break            
            default:
                script.env.rctrl_project = "rctrl"
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-muon-dt/dt-offline/cms-dt-automation:dev"
                script.env.image_setup = "source /home/cmsdtaut/setup.sh"
                script.env.dbinstance = "dt_test_v1"
                script.env.campaign = "express_test_v2"
                script.env.logurl = "https://cms-dt-automation.web.cern.ch/logs"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/3tgrptpc5ibgufpwbtfwqnzksr"
                script.env.eospath = "/eos/cms/store/group/dpg_dt/comm_dt/automation"
                script.env.eosplots = "/eos/project/c/cmsweb/www/MUON/dpgdt/sx5/Results/Automation"
                script.env.plotsurl = "https://dt-sx5.web.cern.ch/dt-sx5/Results/Automation/"
                break
        }
    }
}
